# A re-introduction to Java​Script Notes

## Overview
- Notes from [A re-introduction to Java​Script Notes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript#Strings)
- JavaScript supports object-oriented programming with object prototypes, not classes
  - JavaScript classes, introduced in ECMAScript 2015, are syntactical sugar over function prototypes 
- JavaScript supports functional programming
  - Functions may be stored in variables and can be passed around

## Types
- `Number`
- `String`
- `Boolean`
- `Object`
  - `Function`
  - `Array`
  - `Date`
  - `RegExp`
- `Symbol`
- `null`
- `undefined`

### Numbers
- There's no such thing as an integer
  - `Number`s are floating numbers

### Strings
- API documentation [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)
- There is something called `Template Literals` 
  - `Template Literals` can contain embedded expressions
```javascript
var a = 5;
var b = 10;
console.log(`Fifteen is ${a + b} and not ${2 * a + b}.`);
// Fifteen is 15 and not 20.
````

## Variables
- Blocks do not have scope

> An important difference between JavaScript and other languages like Java is that in JavaScript, blocks do not have scope; only functions have a scope. So if a variable is defined using `var` in a compound statement (for example inside an if control structure), it will be visible to the entire function. However, starting with ECMAScript 2015, `let` and `const` declarations allow you to create block-scoped variables.

## Objects
- `Object`s in JavaScript are name - value pairs
  - Consider HashMap in Java
  - The name is a JavaScript String
  - The value can be any other JavaScript value

```javascript
// Object creation examples
let foo = {};

let bar = new Object();

let koray = {
    name: 'Koray',
    age: 34,
    children: [{
        name: 'Deniz',
        age: 2
    }, {
        name: 'Toprak',
        age: 2
    }]
};
```

### Object Prototypes
```javascript
function Person(name, age) {
    this.name = name;
    this.age = age;
}

let koray = new Person('Koray', 34);

console.log(koray);
console.log(koray['name']);
console.log(koray.age);
````

## Arrays
- API documentation [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
- Frequently used functions [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript#Arrays)
```javascript
let myArr = ['foo', 'bar'];
myArr.push('baz');

for (let val of myArr) {
    console.log(val);
}

// Since ES5
myArr.forEach(function(val, index, array) {
    // Do something with currentValue or array[index]
});
```
- You can `spread` an array as follows
  - This is called the [spread operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
```javascript
function f(firstName, lastName) {
    console.log(firstName.concat(' ').concat(lastName));
}

f(...['Koray', 'Tugay']);  // Spreading values in the array
```

## Functions
- Functions have an array-like local variable called `arguments`
  - Documentation [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments)
```javascript
function f() {
    console.log(arguments);
}

f(1, 2, 3);  // [Arguments] { '0': 1, '1': 2, '2': 3 }
```
- Rest parameter syntax can also be used for accepting an arbitrary number of arguments
```javascript
function f(...args) {
    console.log(args);
}

f(1, 2, 3);  // [ 1, 2, 3 ]
```

- Functions are objects too and you can call the `apply` method on them
  - `apply` takes an array of arguments
```javascript
function f(...args) {
    console.log(args);
}

f.apply(null, [1, 2, 3]);  // [ 1, 2, 3 ]
```

- You can put an anonymous function anywhere you can put an expression
```javascript
let a = 5;

(
    function () {
        let a = 3;
        console.log(a);
    }()
);

console.log(a);

// 3
// 5
```

## Custom Objects
- There is no `class` in JavaScript
  - Functions are used as classes

### Example and `this`
- Inside a function, `this` refers to the current object
- When a method is called using the `.`, `this` refers to the object
  - Otherwise `this` refers to the global object

```javascript
function makePerson(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            return this.firstName.concat(' ').concat(this.lastName);
        }
    };
}

let koray = makePerson('koray', 'tugay');
console.log(koray.fullName());

// This will throw TypeError in runtime
let fullNameFunction = koray.fullName;
fullNameFunction();
// Cannot read property 'concat' of undefined
```

### Constructor Functions
```javascript
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;

    this.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    }
}

let koray = new Person('koray', 'tugay');
console.log(koray.fullName());
```

### Prototypes
```javascript
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.fullName = function () {
    return this.firstName + ' ' + this.lastName;
};


let koray = new Person('koray', 'tugay');
console.log(koray.fullName());
```

## Closures
```javascript
function adderBuilder(foo) {
    return function (baz) {
        return foo + baz;
    }
}

let myAdder = adderBuilder(5);

console.log(myAdder(10));  // Prints 15
```
