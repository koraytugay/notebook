# Class Design

## Abstract Classes
- An `abstract` class itself can not be `final`
  - An `abstract` class can have `final` (and non-abstract) methods
```java
abstract class A {
    final void foo() {}  // This is fine
}
```

## Packages
- `import` is not needed for classes in same package
```java
package kt;

class Foo {
    static void foo() {}
}

package kt;

class Bar {
    // importing Foo is not needed
    void bar() {Foo.foo();}
}
```

## Encapsulation
- You can access a private attribute of another instance of same class within the class itself
```java
class Bar {
    private int bar;
}

class Foo extends Bar {
    private int foo;
    void foo(Foo foo) {
        this.foo = foo.foo;
        // This will not compile
        // this.foo = foo.bar;
    }
}
```

## Inheritance
### Inheritance in Instance Fields
- There is no such thing as inheritance in instance fields
```java
class A {
    String a = "a";
}

class B extends A {
    String a = "b";
}

A a = new A();
A b = new B();

a.a; // a
b.a; // a

new A().a; // a
new B().a; // b
```

### instanceof
- If `T` is `instanceof` `S`, `T[]` is `instanceof` `S[]`
```java
new Integer() instanceof Object;    // true
new Integer[0] instanceof Object[]; // true
```

### Method Name Conflict Between Static and Instance Methods in Inheritance
```java
class Foo {
    static void foo() {}
}

class Bar extends Foo {
    // This will not compile
    // void foo() {}
    // If Foo.foo were private, it would have been fine    
    
    // Remember that this is fine
    static void foo();
}
```

#### Field Name Conflict in Inheritance
- There is no such thing as field name conflict in inheritance
```java
class A {
    int a = 42;
    static int b = 42;
}
class B extends A {
    int a = 84;  // compiles fine
    int b = 84;  // compiles fine
}
```

## Overriding in Inheritance Trees
### Overriding Instance Methods
- An overriding method __can not have__ a more restricting access than the method being overridden
```java
class Foo {
    protected void foo() {}
}

class Bar extends Foo {
    // This will not compile since default is more rectricting than proctected
    // void foo() {}
}
```

#### Overriding Constructors
- An overriding constructor __can have__ a more restricting access than the constructor being overridden
```java
class Foo {
    protected Foo() {}
}

class Bar extends Foo {
    // This is fine
    private Bar() {}
}
```

#### Overriding Static Methods
- Static methods can not be overriden, but can be hidden
```java
class A {
    static void foo() {}
}

class B extends A {
    static void foo() {}  // Hides A.foo
}
```
- Assigning a weaker access in the hiding method is a compilation error
```java
class A {
    static void foo() {}
}

class B extends A {
    // This will not compile!
    // private static void foo() {}
}
```
- Hiding a final static method is a compilation error
```java
class A {
    static final void foo() {}
}

class B extends A {
    // This will not compile
    // static void foo() {}
}
```

## Patterns
### Singleton Class
```java
class MySingleton {
    // The single instance
    private final static MySingleton instance = new MySingleton();

    // Must hide the default constructor
    private MySingleton();

    // Factory method to get the only instance
    public static MySingleton getInstance() {
        return instance;
    }
}
```

### Immutable Class
```java
// class itself is final
final class MyImmutableClass {
    // All fields private and final
    private final int val;
    private final List<Integer> ints;

    // initalised in constructor
    public MyImmutableClass (int val, List<Integer> ints) {
        this.val = val;
        this.ints = ints;
    }

    // getters return copy
    public int getVal() {
        return this.val;
    }
    public List<Integer> getInts() {
        return new ArrayList<>(ints);
    }

    // no setters
}
```
